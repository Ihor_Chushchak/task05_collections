package task05_Collections;

@FunctionalInterface
public interface Application {
    void start();
}
