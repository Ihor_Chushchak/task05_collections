package task05_Collections.queue.view;


import task05_Collections.queue.model.MyDeque;

public class Main {
    public static void startDequeTask() {
        {
            MyDeque<Integer> myDeque = new MyDeque<Integer>();
            for (int element = 0; element < 10; element++) {
                myDeque.insertFirst(element);
                System.out.println(myDeque);
            }
            for (int element = myDeque.size(); element > 0; element--) {
                myDeque.removeFirst();
                System.out.println(myDeque);
            }
        }


    }
}
