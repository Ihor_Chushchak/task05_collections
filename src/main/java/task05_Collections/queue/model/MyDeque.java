package task05_Collections.queue.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class MyDeque<T> implements Deque<T> {
    static Logger logger = LogManager.getLogger(task05_Collections.Main.class);
    private final int INITIAL_CAPACITY = 2;
    private int capacity;
    private T elements[];
    private int first;
    private int last;

    public MyDeque() {
        capacity = INITIAL_CAPACITY;
        elements = (T[]) (new Object[INITIAL_CAPACITY]);
        first = -1;
        last = 0;
    }

    @Override
    public int size() {
        return first + 1;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return (first + 1 == last);
    }

    public T first() {
        if (isEmpty()) {
            logger.trace("You cannot remove an element from an empty double sided queue");
        }
        return elements[first];
    }

    public T last() {
        if (isEmpty()) {
            logger.trace("You cannot remove an element from an empty double sided queue");
        }
        return elements[last];
    }

    public void insertFirst(T element) {
        if (isFull()) {
            doubleExpand();
        }
        elements[first + 1] = element;
        first++;
    }

    public void insertLast(T element) {
        if (isFull()) {
            doubleExpand();
        }
        for (int index = first + 1; index > 0; index--) {
            elements[index] = elements[index - 1];
        }
        elements[0] = element;
        first++;
    }

    public void doubleExpand() {
        T[] array;
        array = (T[]) (new Object[this.size() * 2]);
        for (int element = 0; element < capacity; element++) {
            array[element] = elements[element];
        }
        capacity *= 2;
        elements = array;
    }

    @Override
    public void addFirst(T t) {
    }

    @Override
    public void addLast(T t) {
    }

    @Override
    public boolean offerFirst(T t) {
        return false;
    }

    @Override
    public boolean offerLast(T t) {
        return false;
    }

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            logger.trace("You cannot remove an element from an empty double sided queue");
        }
        if (isQuarterFull()) {
            doubleReduce();
        }
        return elements[first--];
    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            System.err.println("You cannot remove an element from an empty double sided queue");
        }
        if (isQuarterFull()) {
            doubleReduce();
        }
        T last = elements[0];
        for (int index = 0; index < first; index++) {
            elements[index] = elements[index + 1];
        }

        return last;
    }

    public void doubleReduce() {
        T[] array;
        array = (T[]) (new Object[capacity / 2]);
        for (int element = 0; element < size(); element++) {
            array[element] = elements[element];
        }
        capacity /= 4;
        elements = array;
    }

    public boolean isFull() {
        return (size() == capacity);
    }

    public boolean isQuarterFull() {
        return (size() == capacity / 4);
    }

    @Override
    public T pollFirst() {
        return null;
    }

    @Override
    public T pollLast() {
        return null;
    }

    @Override
    public T getFirst() {
        return null;
    }

    @Override
    public T getLast() {
        return null;
    }

    @Override
    public T peekFirst() {
        return null;
    }

    @Override
    public T peekLast() {
        return null;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean offer(T t) {
        return false;
    }

    @Override
    public T remove() {
        return null;
    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }

    @Override
    public void push(T t) {
    }

    @Override
    public T pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public String toString() {
        String string = "{ ";
        for (int index = 0; index <= first; index++) {
            string = string + elements[index] + " ";
        }
        string = string + "}\n";
        string = string + "Size: " + size() + "\n";
        string = string + "Capacity: " + capacity + "\n";
        return string;
    }
}
