package task05_Collections;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {
    private Map<String, String> menu;
    private Map<String, Application> methodsMenu;
    static Logger logger = LogManager.getLogger(Main.class);

    private static final Scanner scanner = new Scanner(System.in);

    public Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - start Priority Queue Task");
        menu.put("2", " 2 - start Container Task");
        menu.put("3", " 3 - start Deque Task");
        menu.put("4", " 4 - start BinTree Task");
        menu.put("5", " 5 - start Array Task");
        menu.put("6", " 6 - Exit");


        methodsMenu.put("1", task05_Collections.priorityqueuedroids.view.Main::startPriorityTask);
        methodsMenu.put("2", task05_Collections.containers.view.Main::startContainerTask);
        methodsMenu.put("3", task05_Collections.queue.view.Main::startDequeTask);
        methodsMenu.put("4", task05_Collections.bintree.view.Main::startBinTreeTask);
        methodsMenu.put("5", task05_Collections.arrays.Main::startArrayTask);
    }

    private void showMenu() {
        for (String value : menu.values()) {
            logger.trace(value);
        }
    }

    public static void main(String[] args) {

        Main main = new Main();
        String keyMenu;

        do {
            main.showMenu();
            keyMenu = scanner.nextLine();
            logger.trace("Select menu point.");
            try {
                main.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                logger.trace(e);
            }
        } while (!keyMenu.equals("4"));
    }
}