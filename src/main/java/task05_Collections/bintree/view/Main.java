package task05_Collections.bintree.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task05_Collections.bintree.module.MyBinTree;
import task05_Collections.PropertiesSetting;

import java.util.Scanner;

public class Main {
    static Logger logger = LogManager.getLogger(task05_Collections.Main.class);
    static int maxRandNum = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("maxRandNum"));
    static int binTreeNodesQuantity = Integer.valueOf(
            new PropertiesSetting().getPropertyValue("binTreeNodesQuantity"));

    public static void startBinTreeTask() {
        MyBinTree<Integer, Integer> myTree = new MyBinTree<>();
        Scanner scanner = new Scanner(System.in);
        int data;
        for (int i = 0; i < binTreeNodesQuantity; i++) {
            int r = (int) (Math.random() * maxRandNum) + 1;
            logger.trace("Insert " + r + "...");
            myTree.put(null, r);
        }
        myTree.print();
        logger.trace("Enter node value to remove: ");
        data = scanner.nextInt();
        myTree.remove(data);
        myTree.print();
    }
}
