package task05_Collections.bintree.module;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

class Node<T> {
    T data;
    Node<T> left, right;

    Node(T data) {
        this.data = data;
    }

    public T getKey() {
        return data;
    }
}

public class MyBinTree<K, T extends Comparable<T>> implements Map<K, T> {
    private Node<T> root;
    private Logger logger = LogManager.getLogger(task05_Collections.Main.class);


    @Override
    public T put(K key, T data) {
        root = put(root, data);
        return null;
    }

    private Node<T> put(Node<T> root, T data) {
        if (root == null) {
            return new Node<T>(data);
        } else if (data.compareTo(root.data) < 0) {
            root.left = put(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            root.right = put(root.right, data);
        } else {
            return root;
        }
        return root;
    }

    @Override
    public T remove(Object data) {
        root = delete(root, (T) data);
        return null;
    }

    private Node<T> delete(Node<T> root, T data) {
        if (root == null) {
            return null;
        } else if (data.compareTo(root.data) < 0) {
            root.left = delete(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            root.right = delete(root.right, data);
        } else {
            if (root.left == null && root.right == null) {
                return null;
            } else if (root.right == null) {
                return root.left;
            } else if (root.left == null) {
                return root.right;
            } else {
                root.data = findMax(root.left);
                root.left = delete(root.left, root.data);
            }
        }
        return root;
    }

    private T findMax(Node<T> root) {
        while (root.right != null) {
            root = root.right;
        }
        return root.data;
    }

    public boolean contains(T data) {
        return contains(root, data);
    }

    private boolean contains(Node<T> root, T data) {
        if (root == null) {
            return false;
        } else if (data.compareTo(root.data) < 0) {
            return contains(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            return contains(root.right, data);
        } else {
            return true;
        }
    }

    public void print() {
        logger.trace("A BinTree is: ");
        print(root);
        System.out.println();
    }

    private void print(Node<T> root) {
        if (root == null)
            return;

        print(root.left);
        System.out.print(" " + root.data);
        print(root.right);
    }

    @Override
    public T get(Object key) {
        Node<T> node = root;
        Comparator<T> comparator = null;
        if (Objects.isNull(node) || Objects.isNull(key)) {
            throw new NullPointerException("The key can't equal null!");
        }
        while (!Objects.isNull(node)) {
            int compare = comparator.compare(node.getKey(), (T) key);
            if (compare > 0) {
                node = node.right;
            } else if (compare < 0) {
                node = node.left;
            } else {
                return node.data;
            }
        }
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public void putAll(Map<? extends K, ? extends T> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<T> values() {
        return null;
    }

    @Override
    public Set<Entry<K, T>> entrySet() {
        return null;
    }
}