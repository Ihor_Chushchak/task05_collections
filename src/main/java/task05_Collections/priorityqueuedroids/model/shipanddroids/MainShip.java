package task05_Collections.priorityqueuedroids.model.shipanddroids;

import task05_Collections.priorityqueuedroids.model.priorityqueue.MyPriorityQueue;

public class MainShip<T extends MainDroid> {
    MyPriorityQueue<T> droid;
    private int maxPrior = 5;

    @Override
    public String toString() {
        return droid.toString();
    }

    public void putDroid(MainDroid mainDroid) {
        droid.add(mainDroid, (int) (Math.random() * ++maxPrior));
    }

    public MyPriorityQueue<T> getDroids() {
        return droid;
    }

    public MainShip() {
        droid = new MyPriorityQueue<T>();
    }

}
