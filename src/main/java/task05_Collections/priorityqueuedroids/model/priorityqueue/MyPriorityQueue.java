package task05_Collections.priorityqueuedroids.model.priorityqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task05_Collections.priorityqueuedroids.model.shipanddroids.MainDroid;

public class MyPriorityQueue<T extends MainDroid> {
    static Logger logger = LogManager.getLogger(task05_Collections.Main.class);

    private Object value;
    private int priority;

    private static int size = 0;
    private static MyPriorityQueue begin;
    private static MyPriorityQueue myPriorityQueue;
    private MyPriorityQueue next;
    private Object T;

    public MyPriorityQueue() {
        myPriorityQueue = begin;
    }

    private void addBegin(Object e, int priority) {
        MyPriorityQueue myPriority = new MyPriorityQueue();
        myPriority.value = e;
        myPriority.priority = priority;
        myPriority.next = begin;
        begin = myPriority;
    }

    public void add(Object e, int priority) {
        if (begin == null) {
            begin = new MyPriorityQueue();
            begin.value = e;
            begin.priority = priority;
            begin.next = null;
        } else if (begin.priority > priority) {
            addBegin(e, priority);
        } else {
            MyPriorityQueue priorityQueue = begin;
            while ((priorityQueue.next != null) && (priorityQueue.priority < priority)) {
                priorityQueue = priorityQueue.next;
            }
            priorityQueue.next = new MyPriorityQueue();
            priorityQueue = priorityQueue.next;
            priorityQueue.value = e;
            priorityQueue.priority = priority;
            priorityQueue.next = null;
        }
        size++;
    }

    public int getSize() {
        return size;
    }

    public Object getValue() {
        Object c = myPriorityQueue.value;
        if (canPop()) {
            myPriorityQueue = myPriorityQueue.next;
        }

        return c;
    }

    public boolean canPop() {
        return myPriorityQueue != null;

    }

    public void setPointerToStart() {
        myPriorityQueue = begin;
    }

    public void show() {
        MyPriorityQueue queue = begin;
        while (queue != null) {
            logger.trace("value: " + queue.value);
            queue = queue.next;
        }
    }

}
