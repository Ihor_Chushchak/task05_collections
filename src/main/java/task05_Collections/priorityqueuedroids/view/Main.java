package task05_Collections.priorityqueuedroids.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task05_Collections.priorityqueuedroids.model.priorityqueue.MyPriorityQueue;
import task05_Collections.priorityqueuedroids.model.shipanddroids.MainDroid;
import task05_Collections.priorityqueuedroids.model.shipanddroids.MainShip;


public class Main {
    static Logger logger = LogManager.getLogger(task05_Collections.Main.class);

    public static void startPriorityTask() {
        MainShip<MainDroid> ship = new MainShip<>();
        ship.putDroid(new MainDroid("bort01"));
        ship.putDroid(new MainDroid("bort02"));
        ship.putDroid(new MainDroid("bort03"));
        ship.putDroid(new MainDroid("bort04"));
        ship.putDroid(new MainDroid("bort05"));

        MyPriorityQueue<MainDroid> list = new MyPriorityQueue<>();
        list.add("bort01", 0);
        list.add("bort02", -1);
        list.add("bort03", 2);
        list.add("bort04", 1);
        list.add("bort05", 5);

        int size = list.getSize();
        for (int i = 0; i < size; i++) {
            if (list.canPop()) {
                logger.trace("Value: " + list.getValue());
            }
        }
    }


}
