package task05_Collections.containers.model.comparators;

import task05_Collections.containers.model.CountryCapital;

import java.util.Comparator;

public class CompareCapital<T> implements Comparator<CountryCapital> {

    @Override
    public int compare(CountryCapital countryCapital1, CountryCapital countryCapital2) {
        return countryCapital1.getCapital().compareTo(countryCapital2.getCapital());
    }


}