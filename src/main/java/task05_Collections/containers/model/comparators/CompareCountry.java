package task05_Collections.containers.model.comparators;

import task05_Collections.containers.model.CountryCapital;

import java.util.Comparator;

public class CompareCountry<T> implements Comparator<CountryCapital> {

    @Override
    public int compare(CountryCapital countryCapital1, CountryCapital countryCapital2) {
        return countryCapital1.getCountry().compareTo(countryCapital2.getCountry());
    }


}