package task05_Collections.containers.view;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task05_Collections.containers.model.Container;
import task05_Collections.containers.model.CountryCapital;
import task05_Collections.containers.model.comparators.CompareCapital;
import task05_Collections.containers.model.comparators.CompareCountry;

import java.util.*;

public class Main {
    static Logger logger = LogManager.getLogger(task05_Collections.Main.class);

    public static CountryCapital getRandomCountry() {

        Random rnd = new Random();
        final int max = 3;
        int choose = rnd.nextInt(max);

        CountryCapital element = new CountryCapital();
        switch (choose) {
            case 0:
                element.setCountry("Ukraine");
                element.setCapital("Kyiv");
                break;
            case 1:
                element.setCountry("Poland");
                element.setCapital("Warsaw");
                break;
            case 2:
                element.setCountry("Belarus");
                element.setCapital("Minsk");
                break;

            default:
                element.setCountry("null");
                element.setCapital("null");
                break;
        }
        return element;
    }

    public static void startContainerTask() {
        Container row = new Container();
        row.add("Earth");
        row.add("Ukraine");
        row.add("Lviv");
        row.add("Epam");
        row.add("Ihor");
        row.showAll();

        final int size = 3;
        CountryCapital[] countryCapitals = new CountryCapital[size];

        logger.trace("Before sorting:");
        for (int i = 0; i < size; i++) {
            countryCapitals[i] = new CountryCapital();
            countryCapitals[i].setAll(getRandomCountry());
            System.out.println(countryCapitals[i]);
        }

        Arrays.sort(countryCapitals, new CompareCountry<>());

        logger.trace("\n\nAfter sorting country:");
        for (int i = 0; i < size; i++) {
            System.out.println(countryCapitals[i]);
        }


        Arrays.sort(countryCapitals, new CompareCapital<>());

        logger.trace("\n\nAfter sorting capital:");
        for (int i = 0; i < size; i++) {
            System.out.println(countryCapitals[i]);
        }

        String key = countryCapitals[2].getCapital();
        int a = Arrays.binarySearch(countryCapitals, new CountryCapital("Ukraine", "Kyiv"), new CompareCapital<>());
        logger.trace("Index with key \"" + key + "\": " + a);

    }


}
